# Prints the n most used colors in palette format from image.
#
# argv[1]: Input image 
# argv[2]: Number of colors
#
# @author Alexandre Prud'Homme
# License: MIT

import sys
from PIL import Image

if len(sys.argv) != 3:
    print("Usage: {} [input-image-file] [number-colors]".format(sys.argv[0]))
    sys.exit(0)

def keyFunc(c):
    return c[0]

listColors = Image.open(sys.argv[1]).convert('RGB').getcolors()

for c,p in sorted(listColors, key=keyFunc, reverse=True)[:int(sys.argv[2])]:
    total = (p[0]<<16) + (p[1]<<8) + p[2]
    print("{0:0{1}x}".format(total,6))
