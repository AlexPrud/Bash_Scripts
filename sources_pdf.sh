#!/usr/bin/env bash
#
# Put source code files in one big PDF file with syntax highlighting and line 
# numbering. The pdf file can be found in the relative directory specified by 
# the FOLDER variable.
#
# Prerequisite: vim
#
# Usage: bash sources_pdf.sh FILE ...
#
# Example: bash sources_pdf.sh file1.h file1.c file2.h file2.c
# Will make a single PDF file with the contents of file1.h, file1.c,
# file2.h and file2.c in this exact order with syntax highlighting
# and line numbering.
#
# Author: Alexandre Prud'Homme
# License: MIT

# vim settings
COLORSCHEME="default"
MARG=16
PRINTOPTIONS='paper:letter,left:'"$MARG"'mm,right:'"$MARG"'mm,top:'"$MARG"'mm,bottom:'"$MARG"'mm,number:n'

# output settings
FORMAT="pdf"
FOLDER="sources_"$FORMAT""
OUTPUT="sources."$FORMAT""

tmpDir="$(mktemp -d)"
count=0

# Make individually formatted PDF files
for file in "$@"; do 
    if [ -f "$file" ]; then
        fileName="$(basename $file)"
        vim -c 'colorscheme '"$COLORSCHEME"'' -c 'set printoptions='"$PRINTOPTIONS"'' -c 'ha > '"$tmpDir"'/'"$fileName"'.'"$FORMAT"'' -c wqa "$file"
        FILES_PDF[$count]=""$tmpDir"/"$fileName"."$FORMAT""
        count+=1
    else
        echo "Warning: "$file" is not a regular file. Ignoring..."
    fi
done

if [ -z "$FILES_PDF" ]; then
    echo "No regular files to process. Aborting..."
    echo "Usage: bash "$0" FILE ..."
    exit 1
fi

if [ ! -d "$FOLDER" ]; then
    mkdir "$FOLDER" || {
        echo "Failed to make directory "$FOLDER". Aborting..."
        exit 1
    }
fi

# Combine them in one big PDF file
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="$FOLDER"/"$OUTPUT" "${FILES_PDF[@]}"

echo Processing done. "$OUTPUT" should be in "$FOLDER"/.
