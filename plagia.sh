# Checks for plagiarism in a set of files.
#
# The output is in columns as follows:
# Percentage of common lines 
# File 1 (relative path)
# File 2 (relative path)
#
# arg1: directory
# arg2: file extension
#
# Usage: bash plagia.sh <directory> <file extension>
# Example: bash plagia.sh myDirectory/ java
#
# Author: Alexandre Prud'Homme
# License: MIT

usage="Usage: bash plagia.sh <directory> <file extension>"

[ $# -ne 2 ] && {
    echo "$usage"
    exit 1
}

[ -d $1 ] || {
    echo "Error: $1 is not a directory"
    echo "$usage"
    exit 1
}

numFiles=0
tmpDir=$(mktemp -d)
OIFS="$IFS"

IFS=$'\n'
for file in $(find "$1" -type f -regex ".*\.$2" | grep -v "/\."); do

    # Save original file name
    fileNameArray[$numFiles]="$file"

    # Remove whitespaces, empty lines and alphabetic characters
    sed "s/\s//g" "${fileNameArray[$numFiles]}" | sed -E "/^$/d" | sed -E 's/[a-zA-Z_]//g' | sort > $tmpDir/$numFiles

    # Save number of lines in each file
    fileNumLines[$numFiles]=$(wc -l $tmpDir/$numFiles | cut -f 1 -d ' ')

    numFiles=$(bc <<< $numFiles+1)
done
IFS="$OIFS"

[ $numFiles -eq 0 ] && {
    echo "No files to check"
    exit 2
}
numFiles=$(bc <<< $numFiles-1)

for i in $(seq 0 $numFiles); do
    for j in $(seq $i $numFiles); do 
        if [ $i -ne $j ]; then
            numCommonLines=$(comm -123 $tmpDir/$i $tmpDir/$j --total | tail -n1 | cut -f3)
            numLinesi=${fileNumLines[$i]}
            numLinesj=${fileNumLines[$j]}
            percentCommLines=$(bc <<< "scale=2;100*$numCommonLines/$(($numLinesi>$numLinesj?$numLinesi:$numLinesj))")
            echo -e "$percentCommLines\t${fileNameArray[$i]}\t${fileNameArray[$j]}" >> $tmpDir/out
        fi
    done
done
cat <(sort -n $tmpDir/out)

