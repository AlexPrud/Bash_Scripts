# Personal Bash Scripts

Bash scripts that are handy in my every day computing.

## AudioSwitcher.sh

AudioSwitcher allows me to switch my audio output device with a bash command instead of the GUI provided by PulseAudio. I use both headphones and speakers and needed a way to quickly switch between both. 

## sources_pdf.sh

Put source code in one big PDF file with syntax highlighting.

## plagia.sh

Checks for plagiarism in a set of files