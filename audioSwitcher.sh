#!/usr/bin/env bash
#
# Author: Alexandre Prud'Homme
#
# Simple script to switch the active and default audio output device through the command line.
# This allows to bind a keyboard shortcut to switch the audio ouput device.
#
# Prerequisite: PulseAudio
# https://www.freedesktop.org/wiki/Software/PulseAudio/
#
# Usage: $ bash AudioSwitcher.sh [index of device]
#
# To get the index(es) and name(s) of the device(s) you want to switch to, use:
# $ bash AudioSwitcher.sh -l
#
# Example: 
#
# 1) List the index(es) of the output audio device(s)
#
# $ bash AudioSwitcher.sh -l
# 0 : alsa_output.pci-0000_01_00.1.hdmi-stereo-extra1
# 1 : alsa_output.usb-FiiO_DigiHug_USB_Audio-01.analog-stereo
# 2 : alsa_output.pci-0000_00_1b.0.analog-stereo
#
# 2) Switch your active and default audio device
# 
# $ bash AudioSwitcher.sh 1

showListAudioDevs() {
    count=0
    for dev in "${audioDevs[@]}"; do
        echo "$count" : "$dev"
        count=$(bc <<< $count+1)
    done
}

changeSinks() {
    # Set default output device 
    pacmd set-default-sink "${audioDevs[$1]}"

    # Setting the default output device is not enough.
    # Active audio streams have to be redirected to the new selected default. 
    listIndexSinkInputs=($(pacmd list-sink-inputs | grep index | grep -oE "[0-9]+"))
    for index in "${listIndexSinkInputs[@]}"; do
        pacmd move-sink-input "$index" "${audioDevs[$1]}"
    done
}

showUsage() {
    echo "Usage: bash audioSwitcher.sh [ -l | index ]"
}


[ "$#" -eq 1 ] || {
    showUsage
    exit 1
} 
    
audioDevs=($(pacmd list-sinks | grep name: | grep -oE "<.*>" | sed -E "s/[<>]//g"))

if [ "$1" = "-l" ]; then
    showListAudioDevs
else

    [[ $1 =~ [0-9]+ ]] && [ "$1" -ge "0" ] && [ "$1" -lt "${#audioDevs[@]}" ] || {
        showUsage
        exit 1
    }

    changeSinks "$1"
fi

exit
