#!/usr/bin/env bash
#
# Calcul du total de la section source, execution et total pour les fichiers 
# de correction markdown produit par Melanie Lord dans le cours INF1120.
#
# Pour adapter ce script à un tp particulier, il ne suffit que de
# spécifier nbr_sections_sources. (Il se peut que d'autres modifications soient
# requises, il est important de valider l'exactitude des calculs)
#
# Utilisation: bash total.sh [ fichier markdown ]
#
# Auteur : Alexandre Prud'Homme
#
let "nbr_sections_sources = 6"

for arg in $@; do
    let "compt = 0"

    SECTIONSNONREMPLIS=$(grep -E '\(/[0-9]+\)' $arg | grep -v "CORRECTION" | grep -v "Note - avant" | wc -l)
    [ $SECTIONSNONREMPLIS -eq 0 ] || {
        echo Erreur: Manque des sections - $arg
        exit 1
    }

    # La commande ci-dessous retourne la liste des notes (chaque section). 
    SECTIONS=$(grep ") #" $arg | tail -n+2 | grep -oE "\([0-9]+\.?[0-9]*" | cut -c 2-)

    # Pour voir cette liste, il ne suffit que de décommenter la ligne ci-dessous.
    #echo $SECTIONS

    TOT_CODE=0
    TOT_EXEC=0

    for i in $SECTIONS;
    do
        if [ $compt -lt $nbr_sections_sources ]
        then
            TOT_CODE=$(bc <<< $TOT_CODE+$i)
            let "compt += 1"
        else
            TOT_EXEC=$(bc <<< $TOT_EXEC+$i)
        fi
    done

    TOT_EXEC_60=$(py "round($TOT_EXEC/88*60,1)")

    echo -----------------------------------------------------
    echo $(sed '2q;d' $arg) 
    echo $(sed '3q;d' $arg) 
    echo -----------------------------------------------------
    echo "Note - avant pénalité : $(bc <<< $TOT_CODE+$TOT_EXEC_60)"
    echo "Total CODE            : $TOT_CODE"
    echo "Total EXEC            : ($TOT_EXEC/88 => $TOT_EXEC_60/60)"
    echo -----------------------------------------------------
done
