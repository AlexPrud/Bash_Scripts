#
# This script takes an image file and reduces the pixel colors to a list of colors.
# 
# argv[1]: Input Image File
# argv[2]: Input Palette File
# argv[3]: Output file
#
# The palette file must be a list of RGB hex codes like so:
#
# 1A2B3C
# DDEEFF
# ...
#
# Dependencies: PIL, colormath (if using DELTA_E) 
#
# Author: Alexandre Prud'Homme
# License: MIT
#

# Max pixel settings
MAX_WIDTH_HEIGHT = 128.0
LIMIT_DIMENSIONS = False 

# Algorithm
DELTA_E = 1
EUCLID = 2
ALGO = EUCLID 

# Use multiple CPUs?
USE_THREADS = True 

import sys
import multiprocessing
import math

from PIL import Image

from ctypes import Structure, c_int

if ALGO == DELTA_E:
    from colormath.color_objects import sRGBColor, LabColor
    from colormath.color_diff import *
    from colormath.color_conversions import convert_color

# Thread settings
if USE_THREADS:
    MAX_THREADS = multiprocessing.cpu_count()

# Default color if no match is found
NO_MATCH_COLOR = (0xFF, 0, 0xFF)

# Class compatible with shared memory
class Pixel(Structure):
    _fields_ = [('r', c_int), ('g', c_int), ('b', c_int)]
    def __str__(self):
        return str((self.r, self.g, self.b))

# Validate number of arguments
if len(sys.argv) != 4:
    print("Usage: {} [input-image-file] [input-palette-file] [output-file]".format(sys.argv[0]))
    sys.exit(0)

# Load RGB values from palette file
colorList_rgb = []
palFile = open(sys.argv[2], "r")
for l in palFile:
    l = l.strip()
    if len(l) != 6:
        continue
    colorList_rgb.append(Pixel(int(l[0]+l[1], 16), int(l[2]+l[3], 16), int(l[4]+l[5], 16)))

# Convert RGB palette values to LAB if using DELTA_E function
colorList_lab = []
if (ALGO == DELTA_E):
    for c in colorList_rgb:
        colorList_lab.append(convert_color(sRGBColor(c.r, c.g, c.b), LabColor))

# Load Image and downsize
im = Image.open(sys.argv[1]).convert(mode='RGB')
if (LIMIT_DIMENSIONS):
    maxDimention = im.width if im.width > im.height else im.height
    if maxDimention > MAX_WIDTH_HEIGHT:
        ratio = maxDimention / MAX_WIDTH_HEIGHT
        im = im.resize((int(im.width/ratio), int(im.height/ratio)))

# Load data on shared memory
px = multiprocessing.Array(Pixel, im.getdata(), lock=False)

# Finds the closest matching color in the list of colors using the delta E
# distance function (more processing time but better results).
#
# @param p pixel to match
#
# @return closest matching color
def findClosestMatch_deltaE(p):
    retColor = NO_MATCH_COLOR
    distance = sys.maxsize
    lab = convert_color(sRGBColor(p.r, p.g, p.b), LabColor)
    index = -1
    retIndex = 0
    for c in colorList_lab:
        index += 1
        distanceTemp = delta_e_cmc(lab, c)
        #distanceTemp = delta_e_cie2000(lab, c)
        if (distanceTemp < distance):
            distance = distanceTemp
            retIndex = index
    return colorList_rgb[retIndex]

# Finds the closest matching color in the list of colors using the euclid
# distance function (less processing time but average result).
#
# @param p pixel to match
#
# @return closest matching color
def findClosestMatch_euclid(p):
    retColor = NO_MATCH_COLOR
    distance = -1
    for c in colorList_rgb:
        distanceTemp = math.pow(p.r-c.r,2)+math.pow(p.g-c.g,2)+math.pow(p.b-c.b,2)
        if (distanceTemp < distance or distance < 0):
            distance = distanceTemp
            retColor = c 
    return retColor

# Process pixels
#
# @param hBegin height where to begin (inclusive)
# @param hEnd height where to end (exclusive)
def processPixels(pBegin, pEnd):
    for i in range(pBegin, pEnd):
        if ALGO == DELTA_E:
            color = findClosestMatch_deltaE(px[i])
        elif ALGO == EUCLID:
            color = findClosestMatch_euclid(px[i])

        px[i] = (color.r, color.g, color.b) if color != None else NO_MATCH_COLOR

# Waits for all threads
#
# @param tds list of threads to wait for
def threadsJoin(tds):
    for t in tds:
        t.join()

threads = []

if (USE_THREADS):
    numPixels = len(px)
    for i in range(MAX_THREADS):
        pBegin = int(numPixels / MAX_THREADS * i)
        pEnd = int(numPixels / MAX_THREADS * (i+1))
        thread = multiprocessing.Process(target=processPixels, args=(pBegin, pEnd,))
        threads.append(thread)
        thread.start()
    threadsJoin(threads)
else:
    processPixels(0, len(px))

newData = []

for i in range(len(px)):
    newData.append((px[i].r, px[i].g, px[i].b))

im.putdata(newData)
im.save(sys.argv[3], "PNG")
